#!/usr/bin/python
# -*- coding: utf-8

# This script performs a true space-time cluster evaluation, which is missing
# in the current SaTScan release. The input files are SaTScan outputs, and it
# gives as result the list of non-overlapping clusters.
#
# https://gitlab.com/tassia.camoes/arbofor/
#
import utils
import os,subprocess
# add dbf as a dependency!!!
import dbf
import fiona

from datetime import datetime

SATSCAN = 'satscan/'
SSBIN = 'bin/satscan_stdc++6_x86_64_64bit'
PRM = SATSCAN+'arbofor.prm'
POP = SATSCAN+'fortaleza.pop'
GEOXY = SATSCAN+'fortaleza_xy.geo'
GEOLL = SATSCAN+'fortaleza_latlon.geo'
SHAPES = "data/SMS/misc/Bairros/fortaleza119.shp" 

class SaTScanRunner():
    """ A class to handle SaTScam processes. """

    def __init__(self,satscan_bin,prm_file,pop,geo_xy,geo_latlon,maxpop):
        """ Initialize the runner with the SaTScan binary path """
        self.satscan_bin = satscan_bin
        self.prm_template = prm_file
        self.pop = pop
        self.geo_xy = geo_xy
        self.geo_latlon = geo_latlon
        self.maxpop = maxpop

    def run_xy_circular(self,cas,begin,end,output):
        print '\nSaTScan analysis run at %s' % datetime.now()
        print '\nXY coordinates with circular window'
        disease,ext = os.path.splitext(os.path.basename(cas))
        output = os.path.join(output,disease+"_xy_circular.txt")

        subprocess.call([self.satscan_bin,self.prm_template,
                         '--CaseFile',cas,'--PopulationFile',self.pop,
                         '--CoordinatesFile',self.geo_xy,
                         '--CoordinatesType',str(0),
                         '--OutputShapefiles','n',
                         '--OutputCartesianGraph','y',
                         '--MaxSpatialSizeInPopulationAtRisk',str(self.maxpop),
                         '--SpatialWindowShapeType',str(0),
                         '--ResultsFile',output,
                         '--StartDate', begin,
                         '--EndDate', end])


    def run_xy_elliptic(self,cas,begin,end,output):
        print '\nSaTScan analysis run at %s' % datetime.now()
        print '\nXY coordinates with elliptic window'
        disease,ext = os.path.splitext(os.path.basename(cas))
        output = os.path.join(output,disease+"_xy_elliptic.txt")

        subprocess.call([self.satscan_bin,self.prm_template,
                         '--CaseFile',cas,'--PopulationFile',self.pop,
                         '--CoordinatesFile',self.geo_xy,
                         '--CoordinatesType',str(0),
                         '--OutputShapefiles','n',
                         '--OutputCartesianGraph','y',
                         '--MaxSpatialSizeInPopulationAtRisk',str(self.maxpop),
                         '--SpatialWindowShapeType',str(1),
                         '--NonCompactnessPenalty',str(2),
                         '--ResultsFile',output,
                         '--StartDate', begin,
                         '--EndDate', end])


    def run_latlon_circular(self,cas,begin,end,output):
        print '\nSaTScan analysis run at %s' % datetime.now()
        print '\nLatLon coordinates with circular window'
        disease,ext = os.path.splitext(os.path.basename(cas))
        output = os.path.join(output,disease+"_latlon_circular.txt")

        subprocess.call([self.satscan_bin,self.prm_template,
                         '--CaseFile',cas,'--PopulationFile',self.pop,
                         '--CoordinatesFile',self.geo_latlon,
                         '--CoordinatesType',str(1),
                         '--OutputShapefiles','y',
                         '--OutputCartesianGraph','n',
                         '--MaxSpatialSizeInPopulationAtRisk',str(self.maxpop),
                         '--SpatialWindowShapeType',str(0),
                         '--ResultsFile',output,
                         '--StartDate', begin,
                         '--EndDate', end])


class SaTScanCluster():
    """ A class to hold clusters resulting from SaTScam analysis. """
    def __init__(self,cluster_id,loc_id,start,end,llr,rel_risk,loc_list=[]):
        self.id = cluster_id
        self.centroid = loc_id
        self.start = datetime.date(datetime.strptime(start, '%Y/%m/%d'))
        self.end = datetime.date(datetime.strptime(end, '%Y/%m/%d'))
        self.llr = llr
        self.rel_risk = rel_risk
        self.locations = loc_list

    def __str__(self):
        print_str =   '''
Cluster #%d:
- Centroid: %s
- Time period: [%s,%s]
- LLR: %f
- Relative risk: %f
- Locations:
[%s]
''' % (self.id, self.centroid,self.start.strftime("%Y/%m/%d"),
       self.end.strftime("%Y/%m/%d"),self.llr,self.rel_risk,
       " ".join(self.locations))

        return print_str

    def set_locations(self,loc_list):
        self.locations = set(loc_list)

    def space_overlap(self,cluster):
        cluster_loc = set(cluster.locations)
        intersection = cluster_loc.intersection(self.locations)
        if intersection:
            print "-- Overlapping locations:",intersection
            return True
        else:
            print "No space overlap"
            return False

    def time_overlap(self,cluster):
        if max(self.start, cluster.start) < min(self.end, cluster.end):
            print "-- Overlapping periods: [%s,%s] -- [%s,%s]" \
                  % (self.start.strftime("%Y/%m/%d"),
                     self.end.strftime("%Y/%m/%d"),
                     cluster.start.strftime("%Y/%m/%d"),
                     cluster.end.strftime("%Y/%m/%d"))
            return True
        else:
            print "No time overlap"
            return False

    def space_time_overlap(self,cluster):
        print "\nChecking time-space overlap (%d,%d)" % (self.id,cluster.id)
        if self.time_overlap(cluster) and self.space_overlap(cluster):
            return True
        else:
            return False

    def export_shapefile(self,shapes,outfile):
        with fiona.open(shapes) as source:
            source_schema = source.schema 
            source_driver = source.driver
            source_crs = source.crs
            print source_crs
            with fiona.open(outfile, 'w',
                            driver=source_driver,
                            crs=source_crs,
                            schema=source_schema) as shpout:
                #print "#"+str(self.id)+" locations: ",self.locations
                for feature in source:
                    area_name = utils.text_to_id(feature['properties']['nome'])
                    #print area_name
                    if area_name in self.locations:
                        #print feature
                        shpout.write(feature)

class SaTScanResult():
    def __init__(self,basename):
        self.basename = basename
        print '\nPost-SaTScan analysis run at %s' %  datetime.now()
        self.clusters_table = dbf.Table(basename+".col.dbf")
        self.locations_table = dbf.Table(basename+".gis.dbf")
        self.clusters_non_overlap_table = self.clusters_table.new(basename+"_non_overlap.col.dbf")
        self.locations_non_overlap_table = self.locations_table.new(basename+"_non_overlap.gis.dbf")
        self.non_overlap_clusters_ids = []

    def extract_clusters(self):
        clusters_list = []
        self.clusters_table.open()
        for c in self.clusters_table:
            cluster = SaTScanCluster(c["cluster"],
                                     c["loc_id"].strip(),
                                     c["start_date"].strip(),
                                     c["end_date"].strip(),
                                     c["llr"],
                                     c["rel_risk"])
            clusters_list.append(cluster)
        self.clusters_table.close()
        self.locations_table.open()
        for cluster in clusters_list:
            #print "loc_table",self.basename+".gis.dbf"
            selection = "select loc_id where cluster==%d" % cluster.id
            locations_result = self.locations_table.query(selection)
            loc_list = [loc["loc_id"].strip() for loc in locations_result]
            #print "#",cluster.id
            #print loc_list
            cluster.set_locations(loc_list)
        self.locations_table.close()
        return clusters_list

    def remove_overlapping(self,selected,cluster_list):
        no_overlap_list = [c for c in cluster_list
                           if not selected.space_time_overlap(c)]
        return no_overlap_list

    def select_and_clean(self,retained,remaining):
        selected = remaining[0]
        retained.append(selected)
        remaining = self.remove_overlapping(selected,remaining)
        print "\nRemaining, after removing overlap with %d:" % selected.id
        print [str(cluster.id) for cluster in remaining]
        return retained,remaining

    def select_non_overlaping(self,ordered_clusters_list):
        retained = []
        remaining = ordered_clusters_list
        print "\nStarting with the full list of clusters:"
        print [str(cluster.id) for cluster in ordered_clusters_list]

        retained,remaining = self.select_and_clean(retained,remaining)
        while remaining:
            retained,remaining = self.select_and_clean(retained,remaining)

        print "Resulting non-overlapping clusters:"
        self.non_overlap_clusters_id = [cluster.id for cluster in retained]
        print self.non_overlap_clusters_id
        return retained

    def write_non_overlapping(self):
        self.clusters_table.open()
        self.clusters_non_overlap_table.open()
        for record in self.clusters_table:
            if record["cluster"] in self.non_overlap_clusters_id:
                self.clusters_non_overlap_table.append(record)
        self.clusters_table.close()
        self.clusters_non_overlap_table.close()
        self.locations_table.open()
        self.locations_non_overlap_table.open()
        for record in self.locations_table:
            if record["cluster"] in self.non_overlap_clusters_id:
                self.locations_non_overlap_table.append(record)
        self.locations_table.close()
        self.locations_table.close()

    def get_non_overlapping_clusters(self):
        clusters_list = self.extract_clusters()
        retained = self.select_non_overlaping(clusters_list)
        self.write_non_overlapping()
        return retained

if __name__ == '__main__':

    for maxpop in [5,10,15,50]:
        output = SATSCAN+'output/maxpop_%d/'%maxpop
        satscan = SaTScanRunner(SSBIN,PRM,POP,GEOXY,GEOLL,maxpop)
    
        #disease_list = ["dengue"]
        disease_list = ["dengue","chikungunya","zika","arbo_07-17","arbo_15-17"]
        #analysis_list = ["_xy_circular","_xy_elliptic","_latlon_circular"]
        analysis_list = ["xy_elliptic"]
        

#        for disease in disease_list:
#            satscan.run_xy_elliptic(SATSCAN+disease+".cas",
#                                    "2007/01/01","2017/12/31",output) 
            #satscan.run_xy_circular(SATSCAN+disease+".cas",
            #                        "2007/01/01","2017/12/31",output) 
            #satscan.run_latlon_circular(SATSCAN+disease+".cas",
            #                            "2007/01/01","2017/12/31",output)
    
        with open(os.path.join(output,"non_overlapping_summary.txt"),'w') as summary:
            for disease in disease_list:
                for analysis in analysis_list:
                    summary.write("\n############## Analysys: %s_%s\n" \
                                  % (disease,analysis))
                    results = SaTScanResult(os.path.join(output,disease+"_"+analysis))
                    retained = results.get_non_overlapping_clusters()
                    summary.write("\nNon-overlapping clusters: [%s]\n" \
                                  % ", ".join([str(cluster.id) for cluster in retained]))
                    for cluster in retained:
                        summary.write(str(cluster))
                        cluster.export_shapefile(SHAPES,
                                                 os.path.join(output,
                                                         "%s_%s_max%d_%d_%s-%s.shp"
                                                         %(disease,analysis,maxpop,cluster.id,
                                                           cluster.start.strftime("%Y%m"),
                                                           cluster.end.strftime("%Y%m") )))
