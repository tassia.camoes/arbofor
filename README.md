# Arbovirus in Fortaleza

This is the public repository of my research project on arbovirus epidemics in
Fortaleza, Brazil.

All the code will be hosted here, and licensed under GPLv3.

## Dependencies

- Satscan
  - https://www.satscan.org/ 
- Automate-save-page-as
  - https://github.com/abiyani/automate-save-page-as
- python modules:
  - seaborn
  - matplotlib
  - sqlite3
  - pandas
  - beautiful-soup
