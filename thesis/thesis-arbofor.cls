%
% apa-jou-supervisor.cls  version 1.0
%
% Tassia C. Araujo, February 2018
% 
% This class redefines apa's journal \maketitle command to include supervisors
% names and affiliations
%
% Finally, I'm not using this class anymore, since apa6 already provide the
% \note command for that.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% When this class was used, I had the following commands at the preamble:
%
%\supervisors{Mathieu Maheu-Giroux* and Kate Zinszer\dag}
%\supervisorsaffil{* Department of Epidemiology, Biostatistics and
%  Occupational Health, McGill University \\
%  \dag École de santé publique de l'Université de Montréal}
%\degree{Thesis presented to obtain the degree of \\
%  \emph{Honours in Environment - Ecological Determinants of Health}}
%

\ProvidesClass{apa-jou-supervisor}

\LoadClass[jou]{apa}

% Adding supervisor-related commands
% Inspired by:
% https://tex.stackexchange.com/questions/196629/adding-supervisor-to-thesis
\newcommand{\@supervisors}{}
\newcommand{\supervisors}[1]{\renewcommand{\@supervisors}{#1}}
\newcommand{\@supervisorsaffil}{}
\newcommand{\supervisorsaffil}[1]{\renewcommand{\@supervisorsaffil}{#1}}
\newcommand{\@degree}{}
\newcommand{\degree}[1]{\renewcommand{\@degree}{#1}}

\def\maketitle{
 \check@author
 \@ifundefined{r@headr}{\def\r@headr{\protect\MakeUppercase{\protect\scriptsize\@title}}}{}
 \@ifundefined{r@headl}{\def\r@headl{\protect\MakeUppercase{\protect\scriptsize\@author}}}{}
 \twocolumn[
  \vspace{0.03in}
  \begin{center}
% title
  {\LARGE \@title}\\
  \vspace{-0.05in}
  \@ifundefined{@authorTwo}{
% one author-affiliation
  \put@one@authaffil{\@author}{\@affil}}{
  \@ifundefined{@authorThree}{
% two authors-affiliations
  \put@two@authaffil{\@authorOne}{\@affilOne}{\@authorTwo}{\@affilTwo}}{
  \@ifundefined{@authorFour}{
% three authors-affiliations
  \@ifundefined{@twofirst}{
% first one, then two
  \put@one@authaffil{\@authorOne}{\@affilOne}\vspace{-0.15in}\\
  \put@two@authaffil{\@authorTwo}{\@affilTwo}{\@authorThree}{\@affilThree}
  }{
% first two, then one
  \put@two@authaffil{\@authorOne}{\@affilOne}{\@authorTwo}{\@affilTwo}\vspace{-0.15in}\\
  \put@one@authaffil{\@authorThree}{\@affilThree}
  }}{
  \@ifundefined{@authorFive}{ % 2006/01/05 as contributed by Aaron Geller
% four authors-affiliations
  \put@two@authaffil{\@authorOne}{\@affilOne}{\@authorTwo}{\@affilTwo}\vspace{-0.15in}\\
  \put@two@authaffil{\@authorThree}{\@affilThree}{\@authorFour}{\@affilFour}
  }{                          % 2006/01/05 beginning of Aaron Geller contribution
  \@ifundefined{@authorSix}{ % -- thp 2006/01/05
% five authors-affiliations
  \put@two@authaffil{\@authorOne}{\@affilOne}{\@authorTwo}{\@affilTwo}\vspace{-0.15in}\\
  \put@two@authaffil{\@authorThree}{\@affilThree}{\@authorFour}{\@affilFour}%
  \vspace{-0.15in}\\ % thp added negative vertical space
  \put@one@authaffil{\@authorFive}{\@affilFive}
  }{                          % 2006/01/05 end of Aaron Geller contribution
% six authors-affiliations
%% --- thp 2006/01/05 beginning of six-author display
  \put@two@authaffil{\@authorOne}{\@affilOne}{\@authorTwo}{\@affilTwo}\vspace{-0.15in}\\
  \put@two@authaffil{\@authorThree}{\@affilThree}{\@authorFour}{\@affilFour}\vspace{-0.15in}\\
  \put@two@authaffil{\@authorFive}{\@affilFive}{\@authorSix}{\@affilSix}
%% --- thp 2006/01/05 end of six-author display
  }}}}}

  \@ifundefined{@note}
   {\vspace{0.07in}}
   {\vspace{0.07in}\\ {\large\@note\vspace{0.07in}}}

  %% Add supervisors to print
  {\bfseries Supervisors: {\@supervisors} \par}
  \vspace*{1ex}
  {{\@supervisorsaffil} \par}
  \vspace{0.20in}

  {{\@degree} \par}
  \vspace{0.24in}

  \@ifundefined{@abstract}
  {\par }
  {\par \parbox{4.6875in}
   {\small \noindent \@abstract
   }\vspace{0.24in}
  }
  \end{center}
 ]
 \pagenumbering{arabic}
 \@ifundefined{@journal}{\thispagestyle{empty}}{%
  \@ifundefined{@vvolume}{\def\@vvolume{\strut}}{}%
  \@ifundefined{@copnum}{\def\@copnum{\strut}}{}%
  \@ifundefined{@ccoppy}{\def\@ccoppy{\strut}}{}%
  \fancyhead{}
  \fancyhead[LO]{\stiny{\@journal}\vspace{-0.15\baselineskip}\\
                 \stiny{\@vvolume}}
  \fancyhead[RO]{\stiny{\@ccoppy}\vspace{-0.15\baselineskip}\\
                 \stiny{\@copnum}}
  \fancyfoot[CO]{\small\rm\thepage}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}
  \thispagestyle{fancy}
 }
 \@ifundefined{@acks}
  {}
  {\begin{figure}[b]
   \parbox{\columnwidth}{\setlength{\parindent}{0.18in}
   \noindent\makebox[\columnwidth]{\vrule height0.125pt width\columnwidth}\vspace*{0.05in}\par
   {\footnotesize\hspace{-0.04in}\@acks\par}}
   \end{figure}}
 \markboth{\hfill\r@headl\hfill}{\hfill\r@headr\hfill}
 \@ifundefined{no@tab}{\let\tabular\apatabular}{}
 \noindent
}
