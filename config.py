# Configuration parameters

CENTROIDS = 'data/SMS/misc/Bairros/fortaleza119_centroids_WGS.csv'
DB = 'arbofor.sqlite3'
FIGURES = 'figures/'
HDI = 'data/SDE/indicededesenvolvimentohumano.csv'
HDI_CATEGORIES = ['hdi','hdi_education','hdi_income','hdi_longevity']
SATSCAN = 'satscan/'
SATSCAN_OUT = 'satscan/output/'
SATSCAN_BIN = 'bin/satscan_stdc++6_x86_64_64bit'
SCHEMA = 'schema.sql'
SIMDA = 'data/SMS/'
SYNONYMS = 'data/neighborhood_synonyms.csv'
CHANGES = 'data/neighborhood_changes.csv'

# this is already extracted from the database
DISEASE_YEAR = {'dengue': range(2007,2019),
                'zika': range(2016,2019),
                'chikungunya': range(2016,2019)}

SIMDA_URL = 'http://tc1.sms.fortaleza.ce.gov.br/simda/'

DISEASE_MONTH_URL = {'dengue': SIMDA_URL+'dengue/tabela-mes-inicio-sintomas?ano=YEAR\&modo=bairro\&classifinold=5\&criterio=3\&format=excel\&extension=xls',
                     'zika': SIMDA_URL+'zika/tabela-mes-inicio-sintomas?ano=YEAR\&modo=bairro\&classifin=14\&criterio=3\&format=excel\&extension=xls',
                     'chikungunya': SIMDA_URL+'chikungunya/tabela-mes-inicio-sintomas?ano=YEAR\&modo=bairro\&classifin=13\&criterio=3\&format=excel\&extension=xls'}

POPULATION_URL = SIMDA_URL+'populacao/faixa?faixaEtaria=2\&ano_pop=YEAR\&regional=\&format=excel\&extension=xls'

DISEASE_AGE_URL = '' # fill it later, the priority is the monthly report

