#!/usr/bin/python
# -*- coding: utf-8

import sys
import csv
import re
import unicodedata

def strip_accents(text):
    try:
        text = unicode(text, 'utf-8')
    except: # unicode is a default on python 3 
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)

def text_to_id(text):
    text = strip_accents(text.strip()).lower()
    text = re.sub('/', '_', text)
    text = re.sub('[ ]+', '_', text)
    text = re.sub('[^0-9a-zA-Z_-]', '', text)
    return text

