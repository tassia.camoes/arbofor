CREATE TABLE neighborhoods (
    id INTEGER primary key,
    name TEXT not null,
    std_name TEXT not null,
    ser TEXT,
    id_sinan TEXT,
    id_sinanw TEXT,
    id_cadsus TEXT,
    cod_ibge TEXT,
    latitude REAL,
    longitude REAL,
    Xepsg29194 REAL,
    Yepsg29194 REAL
);
--CREATE INDEX name on neighborhoods(std_name);
CREATE TABLE changes (
    std_name TEXT not null,
    treated_as INTEGER not null references neighborhoods(id)
);
CREATE TABLE synonyms (
    neighborhood_id INTEGER not null references neighborhoods(id),
    synonym TEXT not null
);
CREATE TABLE hdi (
    neighborhood_id INTEGER not null references neighborhoods(id),
    year INTEGER not null,
    hdi REAL,
    hdi_education REAL,
    hdi_longevity REAL,
    hdi_income REAL,
    primary key (neighborhood_id,year)
);
CREATE TABLE cases_by_month (
    neighborhood_id INTEGER not null references neighborhoods(id),
    disease TEXT not null,
    year INTEGER not null,
    jan INTEGER not null,
    feb INTEGER not null,
    mar INTEGER not null,
    apr INTEGER not null,
    may INTEGER not null,
    jun INTEGER not null,
    jul INTEGER not null,
    aug INTEGER not null,
    sep INTEGER not null,
    oct INTEGER not null,
    nov INTEGER not null,
    dec INTEGER not null,
    total INTEGER not null,
    primary key (neighborhood_id,disease,year)
);
CREATE TABLE cases_by_age (
    neighborhood_id INTEGER not null references neighborhoods(id),
    disease TEXT not null,
    year INTEGER not null,
    "<1" INTEGER,
    "1-4" INTEGER,
    "5-9" INTEGER,
    "10-14" INTEGER,
    "15-19" INTEGER,
    "20-39" INTEGER,
    "40-59" INTEGER,
    "60-69" INTEGER,
    "70-79" INTEGER,
    "80+" INTEGER,
    total INTEGER,
    primary key (neighborhood_id,disease,year)
);
CREATE TABLE population (
    neighborhood_id INTEGER not null references neighborhoods(id),
    year INTEGER not null,
    "<1" INTEGER,
    "1-4" INTEGER,
    "5-9" INTEGER,
    "10-14" INTEGER,
    "15-19" INTEGER,
    "20-39" INTEGER,
    "40-59" INTEGER,
    "60-69" INTEGER,
    "70-79" INTEGER,
    "80+" INTEGER,
    total INTEGER,
    primary key (neighborhood_id,year)
);
