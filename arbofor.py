#!/usr/bin/python
# -*- coding: utf-8

# This script gathers data from multiple sources and digests it for
# the purpuse of the arbofor project: Spatio-temporal analysis of
# arbovirus epidemics in Fortaleza, Brazil.
#
# https://gitlab.com/tassia.camoes/arbofor/
#

import os,sys,subprocess
import re
import csv
import utils
import sqlite3
import bs4
import seaborn as sns
import pandas as pd

from datetime import datetime
from matplotlib import pyplot as plt

# load configuration variables
from config import *

class ArboForDB():
    """ A SQLite database to host Arbofor project data. """

    def __init__(self,path,schema,overwrite=0,download=0):
        """ Create connection to the database, either a new or an existing
            one. """
        self.path = path
        if overwrite:
            try:
                print "Overwriting pre-existing database."
                os.remove(path)
            except:
                pass
            if download:
                self.retrieve_simda_data(SIMDA)
            self.connect()
            self.schema = schema
            self.create_tables()
            self.load_neighborhoods(CENTROIDS,SYNONYMS,CHANGES)
            self.load_hdi(HDI,2015)
            self.load_population(os.path.join(SIMDA,"population/"))
            self.load_cases(os.path.join(SIMDA,"cases/"))
            self.conn.commit()
        else:
            self.connect()

    def connect(self):
        self.conn = sqlite3.connect(self.path)
        self.c = self.conn.cursor()
        
    def close(self):
        """ Commit changes and close connection to the database. """
        self.conn.commit()
        self.conn.close()
    
    def create_tables(self):
        with open(self.schema) as schema:
            self.c.executescript(schema.read())

    def retrieve_simda_data(self):
        """ Retrieve data spreadsheets from SIMDA, based on the url provided
            as constants above. Data will be saved at the Browsers, Donwload
            directory (adjust your settings as desired)."""
        study_years = []
        for disease in DISEASE_YEAR:
            years_list = DISEASE_YEAR[disease]
            study_years = study_years + years_list
            for year in years_list:
                url = DISEASE_MONTH_URL[disease].replace('YEAR',str(year))
                cmd = './automate-save-page-as/save_page_as --save-wait-time \
                       10 --load-wait-time 10 --destination temp.html '+url
                os.system(cmd)
                try:                
                    # Remove temporary file, if created
                    os.remove("temp.html")
                except:
                    pass
        study_years = list(set(study_years))
        for year in study_years:
            url = POPULATION_URL.replace('YEAR',str(year))
            print url
            cmd = './automate-save-page-as/save_page_as --save-wait-time 3 \
                   --load-wait-time 3 --destination temp.html '+url
            os.system(cmd)
            try:                
                # Remove temporary file, if created
                os.remove("temp.html")
            except:
                pass

    def check_consistency(self):
        """ Check consistency in the database. Eg: total of cases should be the
            same as sum of cases by month and sum by age group. """
        pass

    def load_neighborhoods(self,filename,synonyms,changes):
        """ Load neighborhoods table based on a csv with fields:
            X,Y,gid,nome,ser,
            id_sinan,id_sinanw,id_cadsus,cod_ibge,
            Xepsg29194,Yepsg29194
        """
        # Load the changes_list to be able to discard the deprecated
        # neighborhood names (discard title line of changes file)
        changes_list = [line.split(',') for line in
                       [line.strip() for line in
                        open(changes,'rb').readlines()[1:]]]

        with open(filename, 'rb') as source:
            reader = csv.DictReader(source)
            for row in reader:
                std_name = utils.text_to_id(row["nome"])
                # Only add the neighborhood if it is not one of the changes
                if std_name not in [change[0] for change in changes_list]:
                    sql = '''INSERT INTO neighborhoods(id,name,std_name,ser, \
                             id_sinan,id_sinanw,id_cadsus,cod_ibge,latitude,
                             longitude,Xepsg29194,Yepsg29194) VALUES (%s,"%s", \
                             "%s","%s","%s","%s","%s","%s",%s,%s,%s,%s)''' \
                             % (row["gid"],row["nome"],std_name,row["ser"],
                                row["id_sinan"],row["id_sinanw"],
                                row["id_cadsus"],row["cod_ibge"],row["Y"],
                                row["X"],row["Xepsg29194"],row["Yepsg29194"])
                    # print sql
                    try:
                        self.c.execute(sql)
                        print "Added neighborhood",row["nome"]
                    except:
                        print "Failed to add neighborhood %s to the database" \
                               % row["nome"]

        # Add a placeholder for unknown neighborhood
        self.c.execute('''INSERT INTO neighborhoods(id,name,std_name) \
                          VALUES(9999,"ignorado","ignorado")''')
        # Now that valid neighborhoods were loaded, load changes table
        self.load_changes(changes_list)

        # Finally, load synonyms table
        synonyms_list = [line.split(',') for line in
                         [line.strip() for line in
                          open(synonyms,'rb').readlines()]]
        self.load_synonyms(synonyms_list)

    def load_changes(self,changes_list):
        for std_name,treated_as in changes_list:
            neighborhood_id = self.get_neighborhood_id_by_name(treated_as)
            sql = '''INSERT INTO changes(std_name,treated_as) \
                     VALUES ("%s",%d)''' % (std_name,neighborhood_id)
            #print sql
            try:
                self.c.execute(sql)
                print "Added change: %s will be treated as %s" \
                      % (std_name,treated_as)
            except:
                print "Failed to add change for neighborhodd %s to the \
                       database" % std_name

    def load_synonyms(self,synonyms_list):
        for synonyms_group in synonyms_list:
            for idx,std_name in enumerate(synonyms_group):
                neighborhood_id = self.get_neighborhood_id_by_name(std_name)
                if neighborhood_id: break
            del synonyms_group[idx]
            for std_name in synonyms_group:
                sql = '''INSERT INTO synonyms(
                                      neighborhood_id,synonym)
                                      VALUES (%s,"%s")''' \
                                      % (neighborhood_id,std_name)
                #print sql
                try:
                    self.c.execute(sql)
                    print "Added synonym", std_name
                except:
                    print "Failed to add synonym %s to the database" % std_name

    def get_std_name(self,neighborhood_id):
        sql = '''SELECT std_name FROM neighborhoods WHERE id=%d''' \
                 % neighborhood_id
        self.c.execute(sql)
        try:
            std_name = self.c.fetchone()[0]
            return std_name
        except:
            print "No neighborhood found with id %d" % neighborhood_id
            return 0

    def update_neighborhood_population(self,year,neighborhood_id,row):
        # Get whatever info is saved for this neighborhood
        pop = self.get_population_by_age(int(year),neighborhood_id)
        # Then add new data on top of that
        sql = '''UPDATE population \
                 SET "<1" = %d, "1-4" = %d, \
                     "5-9" = %d, "10-14" = %d, \
                     "15-19" = %d, "20-39" = %d, \
                     "40-59" = %d, "60-69" = %d, \
                     "70-79" = %d, "80+" = %d, \
                     total = %d \
                 WHERE (year=%s AND \
                        neighborhood_id=%d)''' \
                 % (row['<1']+pop[0],
                    row['1-4']+pop[1],
                    row['5-9']+pop[2],
                    row['10-14']+pop[3],
                    row['15-19']+pop[4],
                    row['20-39']+pop[5],
                    row['40-59']+pop[6],
                    row['60-69']+pop[7],
                    row['70-79']+pop[8],
                    row['80+']+pop[9],
                    row['total']+pop[10],
                    year,neighborhood_id)
        
        #print sql
        try:
            self.c.execute(sql)
            print "Updated %s population for %s" \
                   % (year,self.get_std_name(neighborhood_id))
        except:
            print "Failed to update %s population for %s" \
                  % (year,self.get_std_name(neighborhood_id))

    def load_neighborhood_population(self,year,row):
        std_name = utils.text_to_id(row["Bairros"])
        if not std_name=="total":
            neighborhood_id = self.get_neighborhood_id_by_name(std_name)
            if neighborhood_id:
                sql = '''INSERT INTO population(neighborhood_id, \
                         year,"<1","1-4","5-9","10-14","15-19", \
                         "20-39","40-59","60-69","70-79","80+", \
                         total) VALUES(%d,%s,%d,%d,%d, %d,%d,%d, \
                         %d,%d,%d,%d,%d)''' \
                         % (neighborhood_id,year,row['<1'],
                            row['1-4'],row['5-9'],row['10-14'],
                            row['15-19'],row['20-39'],row['40-59'],
                            row['60-69'],row['70-79'],row['80+'],
                            row['total'])
                #print sql
                try:
                    # first, try to insert a new record 
                    self.c.execute(sql)
                except:
                    # if insertion fails, it's probably because there was
                    # already a record in the database, so update it
                    self.update_neighborhood_population(year,neighborhood_id,
                                                        row)
 
    def load_population(self,directory):
        """ Load population table based on a directory of spreadsheets
            FAIXA.xls provided by SIMDA. """
        for root, dirnames, filenames in os.walk(directory):
            for f in filenames:
                faixa_xls = os.path.join(root,f)
                string = open(faixa_xls,'r').read() 
                match = re.search(r'<H1>POPULA&Ccedil;&Atilde;O  DETALHADA POR BAIRRO SEGUNDO A FAIXA ET&Aacute;RIA, FORTALEZA, (.*?)</H1>',string)
                if match:
                    year = match.group(1)
                    # 'read_html' returns a list of dataframes
                    data = pd.read_html(faixa_xls)
                    df = data[0]
                    df.columns = ['Bairros','<1','1-4','5-9','10-14','15-19',
                                  '20-39','40-59','60-69','70-79','80+','total']
                    for index,row in df.iterrows():
                        self.load_neighborhood_population(year,row) 
                    print "Loaded population count for",year
                else:
                    print "File %s is not at the expected FAIXA.xls format" \
                           % faixa_xls

    def update_neighborhood_cases(self,disease,year,neighborhood_id,row):
        cases = self.get_cases_by_month(disease,int(year),neighborhood_id)
        sql = '''UPDATE cases_by_month \
                 SET "jan" = %d, "feb" = %d, \
                     "mar" = %d, "apr" = %d, \
                     "may" = %d, "jun" = %d, \
                     "jul" = %d, "aug" = %d, \
                     "sep" = %d, "oct" = %d, \
                     "nov" = %d, "dec" = %d, \
                     total = %d \
                 WHERE (year=%s AND disease="%s" AND\
                        neighborhood_id=%d)''' \
                 % (row['jan']+cases[0],
                    row['feb']+cases[1],
                    row['mar']+cases[2],
                    row['apr']+cases[3],
                    row['may']+cases[4],
                    row['jun']+cases[5],
                    row['jul']+cases[6],
                    row['aug']+cases[7],
                    row['sep']+cases[8],
                    row['oct']+cases[9],
                    row['nov']+cases[10],
                    row['dec']+cases[11],
                    row['total']+cases[12],
                    year,disease,neighborhood_id)
        try:
            self.c.execute(sql)
            print "Updated %s cases for %s in %s" \
                   % (disease,self.get_std_name(neighborhood_id),year)
        except:
            print "Failed to record %s cases for %s in %s" \
                   % (disease,self.get_std_name(neighborhood_id),year)

    def load_neighborhood_cases(self,disease,year,row):
        std_name = utils.text_to_id(row["Bairros"])
        if not std_name=="total":
            neighborhood_id = self.get_neighborhood_id_by_name(std_name)
            if neighborhood_id:
                sql = '''INSERT INTO cases_by_month(\
                         neighborhood_id,disease,year,jan,feb,\
                         mar,apr,may,jun,jul,aug,sep,oct,nov,dec,\
                         total) VALUES(%d,"%s",%s,%d,%d,%d,%d,%d,\
                         %d,%d,%d,%d,%d,%d,%d,%d)''' \
                         % (neighborhood_id,disease,year,
                            row['jan'],row['feb'],row['mar'],
                            row['apr'],row['may'],row['jun'],
                            row['jul'],row['aug'],row['sep'],
                            row['oct'],row['nov'],row['dec'],
                            row['total'])
                #print sql
                try:
                    self.c.execute(sql)
                except:
                    # if insertion fails, try to retrieve it first,
                    # then add new data on top of what was already in the db
                    self.update_neighborhood_cases(disease,year,
                                                   neighborhood_id,row)

    def load_cases(self,directory):
        """ Load cases table based on a directory of spreadsheets MES.xls
            provided by SIMDA. """
        for root, dirnames, filenames in os.walk(directory):
            for f in filenames:
                mes_xls = os.path.join(root,f)
                string = open(mes_xls,'r').read() 
                match = re.search(r'<H4>(.*?) : CASOS POR bairro  DE RESID.NCIA SEGUNDO O M&Ecirc;S DE IN&Iacute;CIO DE SINTOMAS, FORTALEZA, (.*?)</H4>',string)
                if match:
                    disease = utils.text_to_id(match.group(1))
                    year = match.group(2)
                    print "Loading cases for",disease,"in",year
                    # create a new bs4 object from the html data loaded
                    soup = bs4.BeautifulSoup(string,"lxml")
                    # remove all javascript and stylesheet code
                    for script in soup(["script", "style"]):
                        script.extract()
                    # 'read_html' returns a list of dataframes
                    data = pd.read_html(str(soup))
                    df = data[0]
                    df.columns = ['Bairros','jan','feb','mar','apr','may',
                                  'jun','jul','aug','sep','oct','nov','dec',
                                  'total']
                    #print df.head()
                    for index,row in df.iterrows():
                        self.load_neighborhood_cases(disease,year,row)
                else:
                    print "File %s is not at the expected MES.xls format" \
                           % mes_xls

    def load_hdi(self,filename, year=2015):
        """ Load hdi table based on the csv input containing the columns
            Bairros, latitude and longitude. """
        with open(filename, 'rb') as hdi_input:
            reader = csv.DictReader(hdi_input)

            for row in reader:
                # eliminate non-data rows
                if row["Regional"].startswith("REGIONAL"):
                    std_name = utils.text_to_id(row["Bairros"])
                    neighborhood_id = self.get_neighborhood_id_by_name(std_name)
                    if neighborhood_id:
                        sql = '''INSERT INTO hdi(neighborhood_id, year, hdi, \
                                 hdi_education, hdi_longevity, hdi_income) \
                                 VALUES(%d,%s,%s,%s,%s,%s)''' \
                                 % (neighborhood_id,year,row['IDH'],
                                    row['IDH-Educação'],row['IDH-Longevidade'],
                                    row['IDH-Renda'])
                        #print sql
                        try:
                            self.c.execute(sql)
                            print "Added hdi record for neighborhood %s" \
                                   % std_name
                        except:
                            print "Failed to record hdi for neighborhood %s" \
                                   % std_name 

    def get_neighborhood_id_by_name(self,name):
        std_name = utils.text_to_id(name)
        sql = 'SELECT id FROM neighborhoods WHERE std_name="%s"' % (std_name)
        try:
            self.c.execute(sql)
            neighborhood_id = self.c.fetchone()[0]
            return neighborhood_id
        except:
            print 'No neighborhood found for %s, trying synonyms...' % std_name
            sql = 'SELECT neighborhood_id FROM synonyms WHERE synonym="%s"' \
                   % (std_name)
            try:
                self.c.execute(sql)
                neighborhood_id = self.c.fetchone()[0]
                print 'Found %s -> %s' \
                       % (std_name,self.get_std_name(neighborhood_id))
                return neighborhood_id
            except:
                print 'No synonyms found for %s, trying changes...' % std_name
                sql = 'SELECT treated_as FROM changes WHERE std_name="%s"' \
                       % (std_name)
                try:
                    self.c.execute(sql)
                    neighborhood_id = self.c.fetchone()[0]
                    print 'Found %s -> %s' \
                           % (std_name,self.get_std_name(neighborhood_id))
                    return neighborhood_id
                except:
                    print 'No neighborhood known as %s' % std_name
                    return 0

    def get_hdi(self,neighborhood_id,category="hdi"):
        sql = 'SELECT %s FROM hdi WHERE neighborhood_id=%d' \
               % (category,neighborhood_id)
        #print sql
        try:
            self.c.execute(sql)
            hdi = self.c.fetchone()[0]
            return hdi
        except:
            print 'Failed to retrieve %s for neighborhood %s' \
                   % (category,neighborhood_id)
            return 0

    def get_cases_month(self,disease,year):
        sql = 'SELECT std_name,jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec \
               FROM cases_by_month, neighborhoods \
               WHERE cases_by_month.neighborhood_id=neighborhoods.id AND \
               disease="%s" AND year=%d' % (disease,year)
        try:
            self.c.execute(sql)
            cases = [n for n in self.c.fetchall()]
            return cases
        except:
            print 'Failed to retrieve cases in %d for neighborhood %s' \
                   % (year,neighborhood_id)
            return -1

    def get_cases_year(self,disease,year,neighborhood_id="all"):
        if neighborhood_id=="all":
            sql = 'SELECT sum(total) FROM cases_by_month WHERE \
                   disease="%s" and year=%d' % (disease,year)
        else:
            sql = 'SELECT total FROM cases_by_month WHERE neighborhood_id=%d \
                   and disease="%s" and year=%d' \
                   % (neighborhood_id,disease,year)
        try:
            self.c.execute(sql)
            cases = self.c.fetchone()[0]
            return cases
        except:
            print 'Failed to retrieve cases in %d for neighborhood %s' \
                   % (year,neighborhood_id)
            return -1

    def get_cases_by_month(self,disease,year,neighborhood_id="all"):
        if neighborhood_id=="all":
            sql = 'SELECT sum(jan),sum(fev),sum(mar),sum(apr),sum(may), \
                   sum(jun),sum(jul),sum(aug),sum(sep),sum(oct),sum(nov), \
                   sum(dec),sum(total) FROM cases_by_month WHERE \
                   (disease="%s" AND year=%d)' % (disease,year)
        else:
            sql = 'SELECT jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec, \
                   total FROM cases_by_month WHERE (disease="%s" AND year=%d \
                   AND neighborhood_id=%d)' % (disease,year,neighborhood_id)
        try:
            self.c.execute(sql)
            cases_by_month = self.c.fetchone()
            return cases_by_month
        except:
            print 'Failed to retrieve %s cases by month for %s in %d'  \
                   % (disease,neighborhood_id,year)
            return 0

    def get_population_by_age(self,year,neighborhood_id="all"):
        if neighborhood_id=="all":
            sql = 'SELECT sum("<1"),sum("1-4"),sum("5-9"),sum("10-14"), \
                   sum("15-19"),sum("20-39"),sum("40-59"),sum("60-69"), \
                   sum("70-79"),sum("80+"),sum(total) FROM population WHERE \
                   year=%d' % (year)
        else:
            sql = 'SELECT "<1","1-4","5-9","10-14","15-19","20-39","40-59", \
                   "60-69","70-79","80+",total FROM population WHERE \
                   (neighborhood_id=%d AND year=%d)' % (neighborhood_id,year)
        try:
            self.c.execute(sql)
            population_by_age = self.c.fetchone()
            return population_by_age
        except:
            print 'Failed to retrieve population by age group in %d for \
                   neighborhood %s' % (year,neighborhood_id)
            return 0

    def get_population(self,year,neighborhood_id="all"):
        if neighborhood_id=="all":
            sql = 'SELECT sum(total) FROM population WHERE year=%d' \
                   % (year)
        else:
            sql = 'SELECT total FROM population WHERE neighborhood_id=%d and \
                   year=%d' % (neighborhood_id,year)
        try:
            self.c.execute(sql)
            population = self.c.fetchone()[0]
            return population
        except:
            print 'Failed to retrieve population in %d for neighborhood %s' \
                   % (year,neighborhood_id)
            return 0

    def get_incidence(self,disease,year,neighborhood_id="all"):
        population = self.get_population(year,neighborhood_id)
        cases = self.get_cases_year(disease,year,neighborhood_id)
        if population:
            return ((cases*100000.0)/population)
        else:
            print 'Failed to calculate incidence in %d for %s' \
                   % (year,neighborhood_id)
            return 0

    def get_all_neighborhoods_ids(self):
        sql = 'SELECT id FROM neighborhoods'
        try:
            self.c.execute(sql)
            neighborhoods_list = [n[0] for n in self.c.fetchall()]
            return neighborhoods_list
        except:
            print 'Falied to retrieve list of neighborhoods from database'

    def get_neighborhoods_coordinates(self):
        sql = 'SELECT std_name, latitude, longitude, Xepsg29194, Yepsg29194 \
               FROM neighborhoods'
        try:
            self.c.execute(sql)
            coordinates = [(n[0],n[1],n[2],n[3],n[4])
                           for n in self.c.fetchall()]
            return coordinates
        except:
            print 'Failed to retrieve neighborhoods coordinates'

    def get_neighborhoods_population(self,year):
        sql = 'SELECT std_name, total FROM population, neighborhoods \
               WHERE year=%d AND \
               population.neighborhood_id=neighborhoods.id' % (year)
        try:
            self.c.execute(sql)
            population = [(n[0],n[1]) for n in self.c.fetchall()]
            return population
        except:
            print 'Failed to retrieve population in %d for neighborhood %s' \
                   % (year,neighborhood_id)
            return 0

    def get_years(self,disease="all"):
        if disease=="all":
            sql = 'SELECT DISTINCT year FROM cases_by_month'
        else:
            sql = 'SELECT DISTINCT year FROM cases_by_month \
                   WHERE disease="%s"' % (disease)      
        try:
            self.c.execute(sql)
            disease_year_list = [n[0] for n in self.c.fetchall()]
            return disease_year_list
        except:
            print 'Failed to retrieve list of years from database'

    def get_diseases(self):
        sql = 'SELECT DISTINCT disease FROM cases_by_month'
        try:
            self.c.execute(sql)
            disease_year_list = [n[0] for n in self.c.fetchall()]
            return disease_year_list
        except:
            print 'Failed to retrieve list of diseases from database'

    def get_diseases_years(self):
        sql = 'SELECT DISTINCT disease, year FROM cases_by_month ORDER BY \
               disease, year'
        try:
            self.c.execute(sql)
            disease_year_list = [(n[0],n[1]) for n in self.c.fetchall()]
            return disease_year_list
        except:
            print 'Failed to retrieve list of diseases/years from database'

    def plot_incidence_hdi(self,disease,year,hdi_category):
        neighborhoods_list = self.get_all_neighborhoods_ids()    
        hdi = [self.get_hdi(neighborhood_id,hdi_category)
               for neighborhood_id in neighborhoods_list]
        incidence = [self.get_incidence(neighborhood_id,disease,year)
                     for neighborhood_id in neighborhoods_list]

        df = pd.DataFrame(data = {'hdi': hdi, 'incidence': incidence})
        # for now, removing zeros, but there could be a threshold value
        df_no_zeros = (df[df.hdi!=0])[df.incidence!=0]
        if not df_no_zeros.empty:
            plot = sns.lmplot(x="hdi", y="incidence", data=df_no_zeros)
            plot.savefig(os.path.join(FIGURES,
                         "%s%d_%s.png" % (disease,year,hdi_category)))
            plt.close()

    def plot_all_incidence_hdi(self):
        for disease, year in self.get_diseases_years():
            for category in HDI_CATEGORIES:
                self.plot_incidence_hdi(disease,year,category)

    def plot_incidence_by_year(self,begin,end):
        year = []
        disease = []
        incidence = []
        for d, y in self.get_diseases_years():
            if y>=begin and y<=end:
                i = self.get_incidence(d,y)
                year.append(y)
                disease.append(d)
                incidence.append(i)
        df = pd.DataFrame(data = {'year': year, 'disease': disease,
                                  'incidence': incidence})

        if not df.empty:
            plot = sns.factorplot(x='year', y='incidence', hue='disease',
                                  data=df, kind='bar', palette='deep',
                                  legend=False, aspect=1.5)
            plot.set_xticklabels(rotation=45,fontsize=7)
            plot.ax.legend(loc=2)
            plot.ax.set(xlabel='Year',ylabel='Incidence rate (cases/100,000)')
            plot.ax.set_title('Incidence of arboviral diseases in Fortaleza')
            plot.savefig(os.path.join(FIGURES, "incidence_by_year_%d_%d.png"%(begin,end)))
            plt.close()

    def export_satscan_geo(self):
        ''' Creates the SaTScan coordinates file '''
        with open(os.path.join(SATSCAN,'fortaleza_xy.geo'), 'wb') as geoxy:
            with open(os.path.join(SATSCAN,'fortaleza_latlon.geo'), 'wb') as geo:
                coordinates = self.get_neighborhoods_coordinates()
                for (std_name, lat, lon, x, y) in coordinates:
                    if not std_name=="ignorado":
                        geoxy.write('%s %f %f\n' % (std_name,x,y))
                        geo.write('%s %f %f\n' % (std_name,lat,lon))

    def export_satscan_pop(self):
        ''' Creates the SaTScan population file'''
        years = [year for disease, year in self.get_diseases_years()]
        with open(os.path.join(SATSCAN,'fortaleza.pop'), 'wb') as pop:
            for year in sorted(set(years)):
                population = self.get_neighborhoods_population(year)
                for std_name, count in population:
                    pop.write('%s %d %d\n' % (std_name,year,count))

    def export_satscan_cas(self):
        for disease in self.get_diseases():
            with open(os.path.join(SATSCAN,"%s.cas" % disease),'wb') as cas:
                for year in self.get_years(disease):
                    cases = self.get_cases_month(disease,year)
                    for row in cases:
                        std_name = row[0]
                        if not std_name=="ignorado":
                            for month in range(1,13):
                                if int(row[month]) > 0:
                                    cas.write('%s %d %d/%d\n' % (std_name,
                                                                 row[month],
                                                                 year,month))

    def export_satscan_files(self):
        self.export_satscan_geo()
        self.export_satscan_pop()
        self.export_satscan_cas()

if __name__ == '__main__':
    print '\nArboFor run at %s' % datetime.now()
    
    arbo = ArboForDB(DB,SCHEMA,overwrite=0,download=0)
    arbo.plot_incidence_by_year(2007,2017)
#    arbo.plot_all_incidence_hdi()
#    arbo.export_satscan_files()
    arbo.close() 
