#!/usr/bin/python
# -*- coding: utf-8

# This script extracts neighborhood names and HDI indicators from the source
# file provided by the Fortaleza municipality Economic Development Secretary
# (SDE). The output file has neighborhoods names as lowcase identifiers.

import sys
import csv
import utils

SOURCE = '../Data/SDE/indicededesenvolvimentohumano.csv'
OUTPUT = 'CSV/hdi.csv'

with open(OUTPUT, 'wb') as hdi_output:
    writer = csv.writer(hdi_output)
    writer.writerow(['neighborhood','hdi_education','hdi_longevity',
                     'hdi_income','hdi'])
    with open(SOURCE, 'rb') as hdi_input:
        reader = csv.DictReader(hdi_input)
        for row in reader:
            writer.writerow([utils.text_to_id(row["Bairros"]),
                             row['IDH-Educação'],row['IDH-Longevidade'],
                             row['IDH-Renda'],row['IDH']])
