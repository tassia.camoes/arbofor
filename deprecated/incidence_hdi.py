#!/usr/bin/python
# -*- coding: utf-8

# Based on monthly incidence reports and HDI indicators per neighborhood,
# this script produces scatterplots of incidence per HDI indicator. Each
# point is relative to one neighborhood at a particular year, for a
# particular disease, to which incidence was calculated.

import csv
import utils
import pandas as pd
import seaborn as sns

from matplotlib import pyplot as plt

REPORT_PATH = '../Data/SMS-SIMDA-20171119/'
FIGURE_PATH = 'Figures/'
HDI = 'CSV/hdi.csv'
OUTPUT = 'CSV/incidence_hdi.csv'  # not a persistent file

REPORT_DICT = {'ZIKA': ['2016','2017'],
               'CHIKUNGUNYA': ['2016','2017'],
               'DENGUE':  ['2015','2016','2017']}

hdi_df = pd.read_csv(HDI, index_col=0)

synonyms = {'moura_brasil': 'arraial_moura_brasil',
            'sao_gerardo_alagadico': 'sao_gerardo',
            'bairro_de_lourdes': 'de_lourdes',
            'manoel_dias_branco': 'manuel_dias_branco',
            'vicente_pinzon': 'vincente_pinzon',
            'amadeu_furtado': 'amadeo_furtado',
            'bom_sucesso': 'bonsucesso',
            'vila_peri': 'vila_pery',
            'parque_genibau': 'genibau',
            'planalto_airton_senna': 'planalto_ayrton_senna',
            'prefeito_jose_walter': 'prefeito_jose_valter',
            'vila_manoel_satiro': 'manoel_satiro',
            'palmeiras': 'conjunto_palmeiras'}

# Untreated neighborhoods (reported cases, need to be accounted later):
#'olavo_oliveira' -> NEW
#'parque_santa_maria' -> NEW

# No need of treatment (alternative names, but have 0 cases in report)
#'castelao' -> synonym to 'boa_vista', which is already accounted
#'mata_galinha' -> synonym to 'boa_vista', which is already accounted


for key, value in REPORT_DICT.iteritems():
    disease = key
    for year in value:
        disease_report = REPORT_PATH+disease+'-relatorio-incidencia-mes-'+year+'.csv'
        print "\nProducing HDI-incidence graphs for "+disease+" "+year
        
        errors = []
        hits = []
        
        with open(OUTPUT, 'wb') as output:  # note that this file will be overwritten many times
            writer = csv.writer(output)
            writer.writerow(['Neighborhood','Incidence','HDI Education','HDI Longevity','HDI Income','HDI'])
            with open(disease_report, 'rb') as incidence_input:
                reader = csv.reader(incidence_input)
                for row in reader:
                    if (len(row) > 14):
                        neighborhood = row[0]
                        incidence = row[14]
                        if neighborhood and incidence:
                            if (neighborhood not in ['BAIRRO/REGIONAL',
                                                     'SER I','SER II','SER III',
                                                     'SER IV','SER V','SER VI',
                                                     'IGNORADO','FORTALEZA']):
                                neighborhood_id = utils.text_to_id(neighborhood)
                                try:
                                    try:
                                        writer.writerow([neighborhood_id, float(incidence),float(hdi_df.hdi_education[neighborhood_id]),
                                                         float(hdi_df.hdi_longevity[neighborhood_id]),float(hdi_df.hdi_income[neighborhood_id]),
                                                         float(hdi_df.hdi[neighborhood_id]),])
                                    except:
                                        synonym_id = synonyms[neighborhood_id]
                                        writer.writerow([neighborhood_id, float(incidence),float(hdi_df.hdi_education[synonym_id]),
                                                         float(hdi_df.hdi_longevity[synonym_id]),float(hdi_df.hdi_income[synonym_id]),
                                                         float(hdi_df.hdi[synonym_id]),])
                                        
                                    hits.append(neighborhood_id)
                                except:
                                    errors.append(neighborhood_id)
        
        
        print "\nNumber of neighborhoods processed:",len(hits)
        print '\nCheck the folder\''+FIGURE_PATH+'\'for the resulting graphs'

        df = pd.read_csv(OUTPUT, index_col=0)
        
        plot = sns.lmplot(x='HDI Education', y='Incidence', data=df)
        plot.savefig(FIGURE_PATH+'hdi_education_'+disease+year+'.png')
        plt.close()
        
        plot = sns.lmplot(x='HDI Longevity', y='Incidence', data=df)
        plot.savefig(FIGURE_PATH+'hdi_longevity_'+disease+year+'.png')
        plt.close()
        
        plot = sns.lmplot(x='HDI Income', y='Incidence', data=df)
        plot.savefig(FIGURE_PATH+'hdi_income_'+disease+year+'.png')
        plt.close()
        
        plot = sns.lmplot(x='HDI', y='Incidence', data=df)
        plot.savefig(FIGURE_PATH+'hdi_'+disease+year+'.png')
        plt.close()
        
        print "\nNo match for the following neighborhoods (",len(errors),"):"
        print errors
