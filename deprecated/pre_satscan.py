#!/usr/bin/python
# -*- coding: utf-8

# Another standalone script, based on the routine to generate scatterplots, that
# generates input files in the format expected by SaTScan

import csv
import utils
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

REPORT_PATH = '../Data/SMS-SIMDA-20171119/'
SATSCAN_PATH = 'pre-SaTScan/'
CENTROIDS = 'CSV/centroids_hit_hdi.csv'

REPORT_DICT = {'ZIKA': ['2016','2017'],
               'CHIKUNGUNYA': ['2016','2017'],
               'DENGUE':  ['2015','2016','2017']}

synonyms = {'moura_brasil': 'arraial_moura_brasil',
            'sao_gerardo_alagadico': 'sao_gerardo',
            'bairro_de_lourdes': 'de_lourdes',
            'manoel_dias_branco': 'manuel_dias_branco',
            'vicente_pinzon': 'vincente_pinzon',
            'amadeu_furtado': 'amadeo_furtado',
            'bom_sucesso': 'bonsucesso',
            'vila_peri': 'vila_pery',
            'parque_genibau': 'genibau',
            'planalto_airton_senna': 'planalto_ayrton_senna',
            'prefeito_jose_walter': 'prefeito_jose_valter',
            'vila_manoel_satiro': 'manoel_satiro',
            'palmeiras': 'conjunto_palmeiras'}

# Creates an inverted synonyms dictionary
inv_synonyms = {v: k for k, v in synonyms.iteritems()}

# Creates the SaTScan coordinates file
with open(SATSCAN_PATH+'fortaleza.geo', 'wb') as geo:
    with open(CENTROIDS, 'rb') as centroids:
        reader = csv.DictReader(centroids)
        for row in reader:
            neighborhood_id = utils.text_to_id(row["neighborhood"])
            if neighborhood_id in inv_synonyms.keys():
                neighborhood_id = inv_synonyms[neighborhood_id]
            geo.write(neighborhood_id+' '+row['latitude']+' '+row['longitude']+'\n')

# Creates the SaTScan case and population file
for key, value in REPORT_DICT.iteritems():
    disease = key
    print "\nProducing SaTScan inputs for "+disease
    case_file = SATSCAN_PATH+disease+'.cas'
    population_file = SATSCAN_PATH+disease+'.pop'

    with open(case_file, 'wb') as cases_output:
        with open(population_file, 'wb') as populations_output:
            for year in value:
                disease_report = REPORT_PATH+disease+'-relatorio-incidencia-mes-'+year+'.csv'
                print "\nDigesting report from "+year
                
                errors = []
                hits = []
            
                with open(disease_report, 'rb') as incidence_input:
                    reader = csv.reader(incidence_input)
                    for row in reader:
                        if (len(row) > 14 and row[14]):
                            neighborhood = row[0]
                            if neighborhood and (neighborhood not in ['BAIRRO/REGIONAL',
                                                                     'SER I','SER II','SER III',
                                                                     'SER IV','SER V','SER VI',
                                                                     'IGNORADO','FORTALEZA']):
                                total_cases = int(row[13])
                                incidence = float(row[14])
                                if incidence>0:
                                    neighborhood_id = utils.text_to_id(neighborhood)
                                    if neighborhood_id not in ['olavo_oliveira','padre_andrade','parque_santa_maria']: #excluding neighborhood with no coordinates
                                        pop = int(total_cases/(incidence/100000))
                                        populations_output.write(neighborhood_id+' '+year+' '+str(pop)+'\n')
                                        for month in range(1,13):
                                            if int(row[month]) > 0:
                                                cases_output.write(neighborhood_id+' '+row[month]+' '+year+'/'+str(month)+'\n')
                                                
                                        hits.append(neighborhood_id)
                    print "\nNumber of neighborhoods with cases:",len(hits)
